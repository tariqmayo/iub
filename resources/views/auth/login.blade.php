<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">

    <title>Login</title>
    <!-- Loading third party fonts -->
    <link href="http://fonts.googleapis.com/css?family=Arvo:400,700|" rel="stylesheet" type="text/css">
    <link href="{{url('fonts/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <!-- Loading main css file -->
    <link rel="stylesheet" href="{{url('style.css')}}">

<!--[if lt IE 9]>
    <script src="{{url('js/ie-support/html5.js')}}"></script>
    <script src="{{url('js/ie-support/respond.js')}}"></script>
    <![endif]-->

</head>


<body>

<div id="site-content">
    <header class="site-header">
        <div class="primary-header">
            <div class="container">
                <a href="{{url('/courses')}}" id="branding">
                    <img src="{{url('/images/logo.png')}}" alt="Lincoln high School">
                    <h1 class="site-title">Welcome To Login</h1>
                </a> <!-- #branding -->



                <div class="mobile-navigation"> </div>
            </div> <!-- .container -->
        </div> <!-- .primary-header -->


    </header>
</div>

<main class="main-content">

    <div class="fullwidth-block inner-content">
        <div class="container">
            <div class="row">
                <div class="col-md-6" style="margin-left: 25%">
                    <h1 class="section-title" style="text-align: center"> Login Info</h1>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li style="list-style: none; color: lightcoral">{{$error}}</li>

                        @endforeach
                    </ul>

                    <form method="POST" action="{{ route('login') }}" class="contact-form">
                        {{csrf_field()}}


                        <p>
                            <label style="width: 130px;" for="name">Email</label>
                            <input style="width: 300px;" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        </p>
                        <p>
                            <label style="width: 130px; for="email">Password</label>
                            <input style="width: 300px;" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                        </p>

                        <p class="text-right" style="text-align: center">
                            <input style="width: 300px;" type="submit" value="login">
                        </p>

                        <p class="text-right" style="text-align: center">
                            <a style="width: 300px; text-decoration: none" href="{{route('register')}}"> New User</a>
                        </p>

                    </form>
                </div>

            </div>
        </div>
    </div> <!-- .fullwidth-block -->

</main>

<footer class="site-footer">
    <div class="container">
        <div class="row">
            {{--            <div class="col-md-3">--}}
            {{--                <div class="widget">--}}
            {{--                    <h3 class="widget-title">Contact us</h3>--}}
            {{--                    <address>Lincoln High School <br>745 Jewel Ave Street <br>Fress Meadows, NY 1136</address>--}}

            {{--                    <a href="mailto:info@lincolnhighschool.com">info@lincolnhighschool.com</a> <br>--}}
            {{--                    <a href="tel:48942652394324">(489) 42652394324</a>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            {{--            <div class="col-md-3">--}}
            {{--                <div class="widget">--}}
            {{--                    <h3 class="widget-title">Social media</h3>--}}
            {{--                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>--}}
            {{--                    <div class="social-links circle">--}}
            {{--                        <a href="#"><i class="fa fa-facebook"></i></a>--}}
            {{--                        <a href="#"><i class="fa fa-google-plus"></i></a>--}}
            {{--                        <a href="#"><i class="fa fa-twitter"></i></a>--}}
            {{--                        <a href="#"><i class="fa fa-pinterest"></i></a>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            {{--            <div class="col-md-3">--}}
            {{--                <div class="widget">--}}
            {{--                    <h3 class="widget-title">Featured students</h3>--}}
            {{--                    <ul class="student-list">--}}
            {{--                        <li><a href="#">--}}
            {{--                                <img src="dummy/student-sm-1.jpg" alt="" class="avatar">--}}
            {{--                                <span class="fn">Sarah Branson</span>--}}
            {{--                                <span class="average">Average: 4,9</span>--}}
            {{--                            </a></li>--}}
            {{--                        <li><a href="#">--}}
            {{--                                <img src="dummy/student-sm-2.jpg" alt="" class="avatar">--}}
            {{--                                <span class="fn">Dorothy Smith</span>--}}
            {{--                                <span class="average">Average: 4,9</span>--}}
            {{--                            </a></li>--}}
            {{--                    </ul>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            {{--            <div class="col-md-3">--}}
            {{--                <div class="widget">--}}
            {{--                    <h3 class="widget-title">Newsletter</h3>--}}
            {{--                    <p>Aspernatur, rerum. Impedit, deleniti suscipit</p>--}}
            {{--                    <form action="#" class="subscribe">--}}
            {{--                        <input type="email" placeholder="Email Address...">--}}
            {{--                        <input type="submit" class="light" value="Subscribe">--}}
            {{--                    </form>--}}
            {{--                </div>--}}
            {{--            </div>--}}
        </div>

        <div class="copy">Copyright 2020 Lincoln High School. All rights reserved.</div>
    </div>

</footer>

<script src="{{url('js/jquery-1.11.1.min.js')}}"></script>
<script src="{{url('js/plugins.js')}}"></script>
<script src="{{url('js/app.js')}}"></script>

</body>

</html>



{{--@section('content')--}}
{{--<div class="container">--}}
{{--    <div class="row justify-content-center">--}}
{{--        <div class="col-md-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">{{ __('Login') }}</div>--}}

{{--                <div class="card-body">--}}
{{--                    <form method="POST" action="{{ route('login') }}">--}}
{{--                        @csrf--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>--}}

{{--                                @error('email')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">--}}

{{--                                @error('password')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <div class="form-check">--}}
{{--                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

{{--                                    <label class="form-check-label" for="remember">--}}
{{--                                        {{ __('Remember Me') }}--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row mb-0">--}}
{{--                            <div class="col-md-8 offset-md-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    {{ __('Login') }}--}}
{{--                                </button>--}}

{{--                                @if (Route::has('password.request'))--}}
{{--                                    <a class="btn btn-link" href="{{ route('password.request') }}">--}}
{{--                                        {{ __('Forgot Your Password?') }}--}}
{{--                                    </a>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--@endsection--}}
