<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">

    <title>Login</title>
    <!-- Loading third party fonts -->
    <link href="http://fonts.googleapis.com/css?family=Arvo:400,700|" rel="stylesheet" type="text/css">
    <link href="{{url('fonts/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <!-- Loading main css file -->
    <link rel="stylesheet" href="{{url('style.css')}}">

<!--[if lt IE 9]>
    <script src="{{url('js/ie-support/html5.js')}}"></script>
    <script src="{{url('js/ie-support/respond.js')}}"></script>
    <![endif]-->

</head>


<body>

<div id="site-content">
    <header class="site-header">
        <div class="primary-header">
            <div class="container">
                <a href="{{url('/courses')}}" id="branding">
                    <img src="{{url('/images/logo.png')}}" alt="Lincoln high School">
                    <h1 class="site-title">Welcome To Login</h1>
                </a> <!-- #branding -->



                <div class="mobile-navigation"> </div>
            </div> <!-- .container -->
        </div> <!-- .primary-header -->


    </header>
</div>

<main class="main-content">

    <div class="fullwidth-block inner-content">
        <div class="container">
            <div class="row">
                <div class="col-md-6" style="margin-left: 25%">
                    <h1 class="section-title" style="text-align: center"> Register Form</h1>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li style="list-style: none; color: lightcoral">{{$error}}</li>

                        @endforeach
                    </ul>
                    <form method="POST" action="{{ route('register') }}" class="contact-form">
                        {{csrf_field()}}
                        <p>
                            <label style="width: 130px;" for="name">Name</label>
                            <input style="width: 300px;" id="name" type="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        </p>
                        <p>
                            <label style="width: 130px;" for="name">Email</label>
                            <input style="width: 300px;" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        </p>
                        <p>
                            <label style="width: 130px; for="email">Password</label>
                            <input style="width: 300px;" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                        </p>
                        <p>
                            <label style="width: 130px; for="email">Password</label>
                            <input style="width: 300px;" id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </p>
{{--                        <p>--}}
{{--                            <label style="width: 130px; for="email">Password</label>--}}
{{--                            <select name="type" style="width: 300px;" >--}}
{{--                                <option value="Admin"> Admin</option>--}}
{{--                                <option value="Teacher"> Teacher</option>--}}
{{--                                <option value="Student"> Student</option>--}}
{{--                            </select>--}}
{{--                        </p>--}}

                        <p class="text-right" style="text-align: center">
                            <input style="width: 300px;" type="submit" value="Register">
                        </p>
                    </form>
                </div>

            </div>
        </div>
    </div> <!-- .fullwidth-block -->

</main>

<footer class="site-footer">
    <div class="container">
        <div class="row">

            <div class="copy">Copyright 2020 Lincoln High School. All rights reserved.</div>
        </div>

</footer>

<script src="{{url('js/jquery-1.11.1.min.js')}}"></script>
<script src="{{url('js/plugins.js')}}"></script>
<script src="{{url('js/app.js')}}"></script>

</body>

</html>



{{--@extends('layouts.app')--}}

{{--@section('content')--}}
{{--<div class="container">--}}
{{--    <div class="row justify-content-center">--}}
{{--        <div class="col-md-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">{{ __('Register') }}</div>--}}

{{--                <div class="card-body">--}}
{{--                    <form method="POST" action="{{ route('register') }}">--}}
{{--                        @csrf--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>--}}

{{--                                @error('name')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">--}}

{{--                                @error('email')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">--}}

{{--                                @error('password')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row mb-0">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    {{ __('Register') }}--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--@endsection--}}
