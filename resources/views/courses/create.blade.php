@extends('layouts.myapp')
@section('title','Portal  ')
@section('pageTitle','Courses Module')
@section('content')
    <div class="fullwidth-block inner-content">
        <div class="container">
            <div class="row">
                <div class="col-md-6" style="margin-left: 25%">
                    <h1 class="section-title" style="text-align: center"> Enter Course Info</h1>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li style="list-style: none; color: lightcoral">{{$error}}</li>

                        @endforeach
                    </ul>
                    <form action="{{url('/courses/store')}}" method="post" class="contact-form">
                        {{csrf_field()}}
                        <p>
                            <label style="width: 130px;" for="name">Course Name</label>
                            <span class="control"><input name="course_name" type="text" id="coursename" value="{{old('course_name')}}" placeholder="Course name"></span>
                        </p>
                        <p>
                            <label style="width: 130px; for="email">Course Code</label>
                            <span class="control"><input name="course_code" type="text" id="coursecode" value="{{old('course_code')}}" placeholder="Course Code"></span>
                        </p>
                        <p>
                            <label style="width: 130px; for="website">Credit Hours</label>
                            <span class="control"><input  name="credit_hours" type="text" id="credithours" value="{{old('credit_hours')}}" placeholder="Credit Hours"></span>
                        </p>

                        <p class="text-right">
                            <input type="submit" value="Submit">
                        </p>
                    </form>
                </div>

            </div>
        </div>
    </div> <!-- .fullwidth-block -->

@endsection
