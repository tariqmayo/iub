@extends('layouts.myapp')
@section('title','Welcome Page  ')
@section('pageTitle','Courses Module')
@section('content')
    <div class="fullwidth-block inner-content">
        <div class="container" style="width: 1100px;">
            <div class="fullwidth-content">

                @if(session('message'))
                    <p class="alert alert-success" style="color: #4CAF50"> {{session('message')}}</p>
                @endif

                <h2 class="section-title"> All Courses
                    @if(Auth::check())
                    <a style="margin-left: 700px; text-decoration:none; color: inherit;" href="{{url('/courses/create')}}">Create New Course</a>
                    @endif
                </h2>

                    <div class="accordion"  style="border-top: 2px solid dimgrey">
                        <div class="accordion-toggle">
                            <h3 style="width:35%; color: #7D8974;">Course Name </h3>
                            <h3 style="margin-left: 30px;color: #7D8974; width:15%">Course Code  </h3>
                            <h3 style="margin-left: 30px;color: #7D8974; width:15%">Credit Hours</h3>

                            <h3 style="margin-left: 0px;color: #7D8974; width:20%">
                                Action
                            </h3>
                        </div>
                    </div>


                    <div class="accordion" style="border-top: 2px solid dimgrey">
                   @foreach($courses as $course)
                    <div class="accordion-toggle">
                        <h3 style="width:35%">{{$course->course_name}} </h3>
                        <h3 style="margin-left: 30px; width:15%">{{$course->course_code}} </h3>
                        <h3 style="margin-left: 30px; width:15%">{{$course->credit_hours}} </h3>


                        <h3 style="margin-left: 0px; width:20%">
                            @if(Auth::check() && $course->user_id == Auth::user()->id)
                                <a href="{{url('/courses/edit',$course->id)}}">Assign</a>|
                                <a href="{{url('/courses/edit',$course->id)}}">Edit</a>|<a onclick="return confirm('Are you sure you want to delete this item?');" href="{{url('/courses/delete',$course->id)}}">Delete</a>
                            @endif
                        </h3>
                    </div>
                @endforeach
                </div>

            </div>
        </div>
    </div> <!-- .fullwidth-block -->

@endsection
