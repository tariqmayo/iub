@extends('layouts.myapp')
@section('title','Portal  ')
@section('pageTitle','Courses Module')
@section('content')
    <div class="fullwidth-block inner-content">
        <div class="container">
            <div class="row">
                <div class="col-md-6" style="margin-left: 25%">
                    <h1 class="section-title" style="text-align: center"> Enter Program Info</h1>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li style="list-style: none; color: lightcoral">{{$error}}</li>

                        @endforeach
                    </ul>
                    <form action="{{url('/program/store')}}" method="post" class="contact-form">
                        {{csrf_field()}}
                        <p>
                            <label style="width: 130px;" for="name">Program Name</label>
                            <span class="control"><input required  name="name" type="text" id="name" value="{{old('name')}}" placeholder="Program Name"></span>
                        </p>
                        <p class="text-right">
                            <input type="submit" value="Submit">
                        </p>
                    </form>
                </div>

            </div>
        </div>
    </div> <!-- .fullwidth-block -->

@endsection
