@extends('layouts.myapp')
@section('title','Welcome Page  ')
@section('pageTitle','Program Module')
@section('content')
    <div class="fullwidth-block inner-content">
        <div class="container" style="width: 1100px;">
            <div class="fullwidth-content">

                @if(session('message'))
                    <p class="alert alert-success" style="color: #4CAF50"> {{session('message')}}</p>
                @endif

                <h2 class="section-title"> All Programs
                    @if(Auth::check())
                    <a style="margin-left: 650px; text-decoration:none; color: inherit;" href="{{url('/program/create')}}">Create New Program</a>
                    @endif
                </h2>

                    <div class="accordion"  style="border-top: 2px solid dimgrey">
                        <div class="accordion-toggle">
                            <h3 style="width:50%; color: #7D8974;">Course Name </h3>

                            <h3 style="margin-left: 0px;color: #7D8974; width:35%">
                                Action
                            </h3>
                        </div>
                    </div>


                    <div class="accordion" style="border-top: 2px solid dimgrey">
                   @foreach($programs as $program)
                    <div class="accordion-toggle">
                        <h3 style="width:50%">{{$program->name}} </h3>
                        <h3 style="margin-left: 0px; width:20%">
                            @if(Auth::check() && $program->user_id == Auth::user()->id)
                                <a href="{{url('/program/edit',$program->id)}}">Edit</a>|<a onclick="return confirm('Are you sure you want to delete this item?');" href="{{url('/program/delete',$program->id)}}">Delete</a>
                            @endif
                        </h3>
                    </div>
                @endforeach
                </div>

            </div>
        </div>
    </div> <!-- .fullwidth-block -->

@endsection
