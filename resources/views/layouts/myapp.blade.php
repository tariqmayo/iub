<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">

    <title>@yield('title')</title>
    <!-- Loading third party fonts -->
    <link href="http://fonts.googleapis.com/css?family=Arvo:400,700|" rel="stylesheet" type="text/css">
    <link href="{{url('fonts/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <!-- Loading main css file -->
    <link rel="stylesheet" href="{{url('style.css')}}">

    <!--[if lt IE 9]>
    <script src="{{url('js/ie-support/html5.js')}}"></script>
    <script src="{{url('js/ie-support/respond.js')}}"></script>
    <![endif]-->

{{--    for editer textarea--}}
    <script src="{{url('js/tinymce.js')}}"></script>
    <script>
        tinymce.init({
            selector:'#textarea', // textarea id
            height:500,
            menubar:false,
            plugin:[
                'advlist autlink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'undo redo | insert | styleselect | bold italic |alignleft aligncenter alignright aligjustify | bullist numlist outdent indent | link image'

        });
    </script>

</head>


<body>

<div id="site-content">
    <header class="site-header">
        <div class="primary-header">
            <div class="container">
                <a href="{{url('/courses')}}" id="branding">
                    <img src="{{url('/images/logo.png')}}" >
                    <h1 class="site-title">Lincoln High School</h1>
                </a> <!-- #branding -->

                <div class="main-navigation">
                    <button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
                    @if(Auth::check())
                    <ul class="menu">
                        <li class="menu-item current-menu-item"><a href="{{url('home')}}">Home</a></li>
                        <li class="menu-item"><a href="{{url('programs')}}">Programs</a></li>
                        <li class="menu-item"><a href="{{url('courses')}}">Courses</a></li>
                        <li class="menu-item"><a href="{{url('students')}}">Students</a></li>
{{--                        <li class="menu-item"><a href="event.html">Events</a></li>--}}
                        <li class="menu-item">

                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                            </li>
                    </ul> <!-- .menu -->
                    @endif

                    @if(!Auth::check())
                        <ul class="menu">

                            <li class="menu-item">

                                <a class="dropdown-item" href="{{ route('login') }}"> Login </a>

                            </li>
                        </ul> <!-- .menu -->
                    @endif
                </div> <!-- .main-navigation -->

                <div class="mobile-navigation"></div>
            </div> <!-- .container -->
        </div> <!-- .primary-header -->

        <div class="home-slider">
            <div class="container">
                <div class="slider" style="margin-bottom: 30px">
                    <ul class="slides">
                        <li>
                            <div class="slide-caption">
                                @if(Auth::check())
                                <h2 class="slide-title">Welcome <br> {{ Auth::user()->name }}<br></h2>
                                @endif
                            </div>
                        </li>

                    </ul> <!-- .slides -->
                </div> <!-- .slider -->
            </div> <!-- .container -->
        </div> <!-- .home-slider -->
    </header>
</div>

<main class="main-content">
@yield('content')

</main>

<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="widget">
                    <h3 class="widget-title">Contact us</h3>
                    <address>Lincoln High School <br>745 Jewel Ave Street <br>Fress Meadows, NY 1136</address>

                    <a href="mailto:info@lincolnhighschool.com">info@lincolnhighschool.com</a> <br>
                    <a href="tel:48942652394324">(489) 42652394324</a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="widget">
                    <h3 class="widget-title">Social media</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    <div class="social-links circle">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-pinterest"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="widget">
                    <h3 class="widget-title">Featured students</h3>
                    <ul class="student-list">
                        <li><a href="#">
                                <img src="dummy/student-sm-1.jpg" alt="" class="avatar">
                                <span class="fn">Sarah Branson</span>
                                <span class="average">Average: 4,9</span>
                            </a></li>
                        <li><a href="#">
                                <img src="dummy/student-sm-2.jpg" alt="" class="avatar">
                                <span class="fn">Dorothy Smith</span>
                                <span class="average">Average: 4,9</span>
                            </a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="widget">
                    <h3 class="widget-title">Newsletter</h3>
                    <p>Aspernatur, rerum. Impedit, deleniti suscipit</p>
                    <form action="#" class="subscribe">
                        <input type="email" placeholder="Email Address...">
                        <input type="submit" class="light" value="Subscribe">
                    </form>
                </div>
            </div>
        </div>

        <div class="copy">Copyright 2014 Lincoln High School. All rights reserved.</div>
    </div>

</footer>

<script src="{{url('js/jquery-1.11.1.min.js')}}"></script>
<script src="{{url('js/plugins.js')}}"></script>
<script src="{{url('js/app.js')}}"></script>

</body>

</html>
