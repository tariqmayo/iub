@extends('layouts.myapp')
@section('title','Portal  ')
@section('pageTitle','Courses Module')
@section('content')
    <div class="fullwidth-block inner-content">
        <div class="container">
            <div class="row">
                <div class="col-md-6" style="margin-left: 25%">
                    <h1 class="section-title" style="text-align: center"> Update Student Info</h1>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li style="list-style: none; color: lightcoral">{{$error}}</li>

                        @endforeach
                    </ul>
                    <form action="{{url('/student/update')}}" method="post" class="contact-form">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="{{$student->id}}">
                        <p>
                            <label style="width: 130px;" for="name">Student Name</label>
                            <span class="control"><input required="required"  name="name" type="text" id="name" value="{{$student->name}}" placeholder="Student Name"></span>
                        </p>
                        <p>
                            <label style="width: 130px; for="email">Roll No</label>
                            <span class="control"><input required="required"  name="rollno" type="text" id="rollno" value="{{$student->rollno}}" placeholder="Roll No"></span>
                        </p>
                        <p>
                            <label style="width: 130px; for="website">Email</label>
                            <span class="control"><input required="required"   name="email" type="email" id="email" value="{{$student->email}}" placeholder="Email"></span>
                        </p>
                        <p>
                            <label style="width: 130px; for="website">Class</label>
                            <span class="control">
                                <select name="class" style="width: 425px;">
                                    <option value='MCS'  @if($student->class == 'MCS')selected="selected"@endif>MCS</option>
                                    <option value='BSCS' @if($student->class == 'BSCS')selected="selected"@endif>BSCS</option>
                                    <option value='BSIT' @if($student->class == 'BSIT')selected="selected"@endif>BSIT</option>

                                </select>
                            </span>
                        </p>
                        <p>
                            <label style="width: 130px; for="website">Semester</label>
                            <span class="control">
                                <select name="semester" style="width: 425px;">
                                    <option value='1' @if($student->semester == '1')selected="selected"@endif>1</option>
                                    <option value='2' @if($student->semester == '2')selected="selected"@endif>2</option>
                                    <option value='3' @if($student->semester == '3')selected="selected"@endif>3</option>
                                    <option value='4' @if($student->semester == '4')selected="selected"@endif>4</option>
                                    <option value='5' @if($student->semester == '5')selected="selected"@endif>5</option>
                                    <option value='6' @if($student->semester == '6')selected="selected"@endif>6</option>
                                    <option value='7' @if($student->semester == '7')selected="selected"@endif>7</option>
                                    <option value='8' @if($student->semester == '8')selected="selected"@endif>8</option>
                                </select>
                            </span>
                        </p>

                        <p class="text-right">
                            <input type="submit" value="Update">
                        </p>
                    </form>
                </div>

            </div>
        </div>
    </div> <!-- .fullwidth-block -->

@endsection
