@extends('layouts.myapp')
@section('title','Welcome Page  ')
@section('pageTitle','Courses Module')
@section('content')
    <div class="fullwidth-block inner-content">
        <div class="container" style="width: 1250px;">
            <div class="fullwidth-content">

                @if(session('message'))
                    <p class="alert alert-success" style="color: #4CAF50"> {{session('message')}}</p>
                @endif

                <h2 class="section-title"> All Students
                    @if(Auth::check())
                        <a style="margin-left: 850px; text-decoration:none; color: inherit;" href="{{url('/student/create')}}">Add New Student</a>
                    @endif
                </h2>
                    <div class="accordion"  style="border-top: 2px solid dimgrey">
                            <div class="accordion-toggle">
                                <h3 style="width:15%; color: #7D8974;">Student Name </h3>
                                <h3 style="margin-left: 30px;color: #7D8974; width:8%">Roll No  </h3>
                                <h3 style="margin-left: 30px;color: #7D8974; width:15%">Email</h3>
                                <h3 style="margin-left: 30px;color: #7D8974; width:7%">Program </h3>
                                <h3 style="margin-left: 15px;color: #7D8974; width:10%">Semester </h3>
                                <h3 style="margin-left: 15px;color: #7D8974; width:10%">Photo </h3>

                                <h3 style="margin-left: 0px;color: #7D8974; width:10%">
                                   Action
                                </h3>
                            </div>
                    </div>

                <div class="accordion" style="border-top: 2px solid dimgrey">
                    @foreach($students as $student)
                        <div class="accordion-toggle">
                            <h3 style="width:15%">{{$student->name}} </h3>
                            <h3 style="margin-left: 30px; width:8%">Roll No : {{$student->rollno}} </h3>
                            <h3 style="margin-left: 30px; width:15%">{{$student->email}} </h3>
                            <h3 style="margin-left: 30px; width:7%">{{$student->class}} </h3>
                            <h3 style="margin-left: 15px; width:10%">Semester : {{$student->semester}} </h3>
                            <h3 style="margin-left: 15px; width:10%"><img style="height: 70px; width: 90px" src="images/stdimages/{{$student->photo}}" /> </h3>

                            <h3 style="margin-left: 0px; width:10%">
                                @if(auth()->user()->hasRole('teacher'))
                                    <a href="{{url('/student/edit',$student->id)}}">Edit</a>|<a onclick="return confirm('Are you sure you want to delete this student?');" href="{{url('/student/delete',$student->id)}}">Delete</a>
                                @endif
                            </h3>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>
    </div> <!-- .fullwidth-block -->

@endsection
