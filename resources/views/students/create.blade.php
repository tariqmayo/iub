@extends('layouts.myapp')
@section('title','Portal  ')
@section('pageTitle','Courses Module')
@section('content')
    <div class="fullwidth-block inner-content">
        <div class="container">
            <div class="row">
                <div class="col-md-6" style="margin-left: 25%">
                    <h1 class="section-title" style="text-align: center"> Enter Student Info</h1>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li style="list-style: none; color: lightcoral">{{$error}}</li>

                        @endforeach
                    </ul>
                    <form action="{{url('/student/store')}}" method="post" class="contact-form" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <p>
                            <label style="width: 130px;" for="name">Student Name</label>
                            <span class="control"><input required="required"  name="name" type="text" id="name" value="{{old('name')}}" placeholder="Student Name"></span>
                        </p>
                        <p>
                            <label style="width: 130px; for="email">Roll No</label>
                            <span class="control"><input required="required"  name="rollno" type="text" id="rollno" placeholder="Roll No"></span>
                        </p>
                        <p>
                            <label style="width: 130px; for="website">Email</label>
                            <span class="control"><input  name="email" type="email" id="email" required="required" placeholder="Email"></span>
                        </p>
                        <p>
                            <label style="width: 130px; for="website">Program</label>
                            <span class="control">
                                <select name="program" style="width: 425px;">
                                    @foreach($programs as $program )
                                                            <option value="{{$program->id}}">{{$program->name}}</option>
                                    @endforeach

                                </select>
                            </span>
                        </p>
                        <p>
                            <label style="width: 130px; for="website">Semester</label>
                            <span class="control">
                                <select name="semester" style="width: 425px;">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                </select>
                            </span>
                        </p>

                        <p>
                            <label style="width: 130px; for="email">Photo</label>
                            <span class="control"><input required="required"  name="photo" type="file" id="photo" placeholder="Upload Your Photo"></span>
                        </p>

                        <p class="text-right">
                            <input type="submit" value="Submit">
                        </p>
                    </form>
                </div>

            </div>
        </div>
    </div> <!-- .fullwidth-block -->

@endsection
