<?php

namespace App\Http\Controllers;

use App\Program;
use App\Student;
use Illuminate\Http\Request;
use File;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students   =   Student::all();
        return  view('students.index',compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $programs   =   Program::all();
        return view('students.create',compact('programs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'name' => 'required|max:255',
            'rollno' => 'required|max:255',
            'email'=> 'required|unique:students',
            'photo' =>  'required'
        ]);


        if ($validate) {

            if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
                $extension = $request->photo->extension();

                $filename = time() . '.' . $extension;
                $target_path = public_path('/images/stdimages/');

                if ($request->file('photo')->move($target_path, $filename)) {


                    $student = new Student;
                    $student->name = $request->name;
                    $student->rollno = $request->rollno;
                    $student->email = $request->email;
                    $student->program_id = $request->program;
                    $student->semester = $request->semester;
                    $student->photo = $filename;
                    $created = $student->save();
                    if ($created) {
                        return redirect('students')->with('message', 'Student Record Added Successfully.');

                    } else {
                        return redirect('student/create')
                            ->withErrors($validate)
                            ->withInput();
                    }

                }

            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student    =   Student::find($id);

        $image_path = "images/stdimages/".$student->photo;  // Value is not URL but directory file path
        if(File::exists($image_path)) {
            File::delete($image_path);
            $deleted    =   $student->delete();
            if($deleted){
                return  redirect('/students')->with('message','Student Record Deleted Successfully!');
            }
        }else{
            return  redirect('/students')->with('message','Student Photo not Exists!');
        }

    }
}
