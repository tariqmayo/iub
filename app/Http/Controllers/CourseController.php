<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses   =   Course::all();
        return view('courses.index',compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'course_name' => 'required|unique:courses|max:255',
            'course_code' => 'required|unique:courses|max:255',
            'credit_hours'=> 'required'
        ]);

        if ($validate) {
            $course                     =   new Course();
            $course->course_name        =   $request->course_name;
            $course->course_code        =   $request->course_code;
            $course->credit_hours       =   $request->credit_hours;
            $course->user_id            =   auth()->user()->id;
            $created                    =   $course->save();
            if($created){
                return redirect('courses')->with('message','New Course Added Successfully.');

            }else{
                return redirect('course/create')
                    ->withErrors($validate)
                    ->withInput();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course   =   Course::find($id);
        return      view('courses.edit', compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {

        $validate = $request->validate([
            'course_name' => 'required|max:255',
            'course_code' => 'required|max:255',
            'credit_hours'=> 'required'
        ]);



        if ($validate) {
            $course                     =   Course::find($request->id);
            $course->course_name        =   $request->course_name;
            $course->course_code        =   $request->course_code;
            $course->credit_hours       =   $request->credit_hours;
            $updated                    =   $course->save();
            if($updated){
                return redirect('courses')->with('message','Course Updated Added Successfully.');

            }else{
                return redirect('course/edit')
                    ->withErrors($validate)
                    ->withInput();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course   =   Course::find($id);
        $deleted    =   $course->delete();
        if($deleted){
            return  redirect('/courses')->with('message','Course Deleted Successfully!');
        }
    }
}
