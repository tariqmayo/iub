<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// routes for program module
Route::get('/programs','ProgramController@index');
Route::get('/program/create','ProgramController@create');
Route::post('/program/store','ProgramController@store');
Route::post('/program/update','ProgramController@update');
Route::get('/program/edit/{id}','ProgramController@edit');
Route::get('/program/delete/{id}','ProgramController@destroy');


// routes for course module
Route::get('/courses','CourseController@index');
Route::get('/courses/create','CourseController@create');
Route::post('/courses/store','CourseController@store');
Route::post('/courses/update','CourseController@update');
Route::get('/courses/edit/{id}','CourseController@edit');
Route::get('/courses/delete/{id}','CourseController@destroy');


Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');


// routes for students module
Route::get('/students','StudentController@index');
Route::get('/student/create','StudentController@create');
Route::post('/student/store','StudentController@store');
Route::post('/student/update','StudentController@update');
Route::get('/student/edit/{id}','StudentController@edit');
Route::get('/student/delete/{id}','StudentController@destroy');

//routes for teacher module
Route::get('/home', 'TeacherController@dashboard');
Route::get('/teacher','TeacherController@index');

// routes for admin module
Route::get('/admin','AdminController@index');
